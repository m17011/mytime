#include <stdio.h>
#include <stdlib.h>
#include "myTime.h"
#include "testCommon.h"
#define N 3
#define HH 0
#define MM 1
#define SS 2


void testGetSeconds() {
	int ans;
	testStart("getSeconds");
	ans = getSeconds(2, 34, 56);
		assertEqualsInt(ans, 2*3600+34*60+56);
	ans = getSeconds(11, 59, 59);
		assertEqualsInt(ans, 12*3600-1);
}

void testGetSecondsFromTime() {
	int jikoku1[N] = {2, 34, 56}, jikoku2[N] = {11, 59, 59};
	int ans;
	testStart("getSecondsFromTime");
	ans = getSecondsFromTime(jikoku1);
		assertEqualsInt(ans, 2*3600+34*60+56);
	ans = getSecondsFromTime(jikoku2);
		assertEqualsInt(ans,12*3600-1);
}

void testMakeTimeFromSeconds() {
int jikoku[N];
testStart("makeTimeFromSeconds");
	makeTimeFromSeconds(jikoku, 2 * 3600 + 34 * 60 + 56);
		assertEqualsInt(jikoku[HH], 2);
		assertEqualsInt(jikoku[MM], 34);
		assertEqualsInt(jikoku[SS], 56);
	makeTimeFromSeconds(jikoku, 11*3600 + 59*60 + 59);
		assertEqualsInt(jikoku[HH], 11);
		assertEqualsInt(jikoku[MM], 59);
		assertEqualsInt(jikoku[SS], 59);
}

void testMakeTime(){
	int jikoku[N];
	testStart("makeTime");
	makeTime(jikoku, 2, 34, 56);
		assertEqualsInt(jikoku[HH], 2);
		assertEqualsInt(jikoku[MM], 34);
		assertEqualsInt(jikoku[SS], 56);
	makeTime(jikoku, 11, 59, 59);
		assertEqualsInt(jikoku[HH], 11);
		assertEqualsInt(jikoku[MM], 59);
		assertEqualsInt(jikoku[SS], 59);
}

void testTimeCmp(){
	int ans1,ans2, t1p[N] = {2, 34, 56}, t2p[N] = {11, 59, 59};
	testStart("timeCmp");
	ans1 = timeCmp(t1p, t2p);
		assertEqualsInt(ans1, 1);
	ans2 = timeCmp(t1p, t1p);
		assertEqualsInt(ans2, 0);

}

void testAddTime(){
	testStart("addTime");
	int jikokua[N], jikoku1[N] = {2, 34, 56}, jikoku2[N] = {1, 2, 34}, jikoku3[N] = {11, 59, 59};
	addTime(jikokua, jikoku1, jikoku2);
	assertEqualsInt(jikokua[HH], 3);
	assertEqualsInt(jikokua[MM], 37);
	assertEqualsInt(jikokua[SS], 30);
	addTime(jikokua, jikoku1, jikoku3);
	assertEqualsInt(jikokua[HH], 14);
	assertEqualsInt(jikokua[MM], 34);
	assertEqualsInt(jikokua[SS], 55);
	}

void testMultime(){
	testStart("mulTime");
	int jikokua[N], jikoku1[N] = {2, 34, 56}, jikoku2[N] = {11, 59, 59}, n=2;
	mulTime(jikokua, jikoku1, n);
	assertEqualsInt(jikokua[HH], 5);
	assertEqualsInt(jikokua[MM], 9);
	assertEqualsInt(jikokua[SS], 52);
	mulTime(jikokua, jikoku2, n);
	assertEqualsInt(jikokua[HH], 23);
	assertEqualsInt(jikokua[MM], 59);
	assertEqualsInt(jikokua[SS], 58);
}

void testDivtime(){
	testStart("divTime");
	int jikokua[N], jikoku1[N] = {2, 34, 56}, jikoku2[N] = {11, 59, 59}, n=2;
	divTime(jikokua, jikoku1, n);
	assertEqualsInt(jikokua[HH], 1);
	assertEqualsInt(jikokua[MM], 17);
	assertEqualsInt(jikokua[SS], 28);
	divTime(jikokua, jikoku2, n);
	assertEqualsInt(jikokua[HH], 5);
	assertEqualsInt(jikokua[MM], 59);
	assertEqualsInt(jikokua[SS], 59);
}
	
void testDivTimeByTime(){
	testStart("divTimeByTime");
	int ans, jikoku1[N] = {2, 34, 56}, jikoku2[N] = {11, 59, 59};
	ans = divTimeByTime(jikoku1, jikoku2);
	assertEqualsInt(ans, 0);
	ans = divTimeByTime(jikoku2, jikoku1);
	assertEqualsInt(ans, 4);
}
void testSubTime(){
	testStart("subTime");
	int jikokua[N], jikoku1[N] = {1, 34, 12}, jikoku2[N] = {2, 56, 30}, jikoku3[N] = {11, 59, 59};
	subTime(jikokua, jikoku2, jikoku1);
	assertEqualsInt(jikokua[HH], 1);
	assertEqualsInt(jikokua[MM], 22);
	assertEqualsInt(jikokua[SS], 18);
	subTime(jikokua, jikoku3, jikoku1);
	assertEqualsInt(jikokua[HH], 10);
	assertEqualsInt(jikokua[MM], 25);
	assertEqualsInt(jikokua[SS], 47);
	}
	

/* テストを呼び出すメインプログラム */
int main() {
	testGetSeconds();
	testGetSecondsFromTime();
	testMakeTimeFromSeconds();
	testMakeTime();
	testTimeCmp();
	testAddTime();
	testMultime();
	testDivtime();
	testDivTimeByTime();
	testSubTime();
	testErrorCheck(); // 消さないこと
	return 0;
}
