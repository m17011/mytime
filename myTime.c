#include <stdio.h>
#include <stdlib.h>
#include "myTime.h"
#define N 3
#define HH 0
#define MM 1
#define SS 2

//int jikoku[N];


int getSeconds(int h, int m, int s){
	int ans;
	ans = h*3600+m*60+s;
	return ans;
}

int getSecondsFromTime(int *tp){
	int ans = 0;
	ans = tp[0]*3600 + tp[1]*60 + tp[2];
	return ans;
}

void makeTimeFromSeconds(int *ansp, int seconds){
	ansp[HH] = seconds / 3600 ;
	ansp[MM] = seconds / 60 % 60 ;
	ansp[SS] = seconds % 60 ;
	
}

void makeTime(int *ansp, int h, int m, int s){
	makeTimeFromSeconds(ansp, getSeconds(h, m, s));
}

int timeCmp(int *t1p, int *t2p){
	
	if(t1p>t2p){
		return 1;
	}
	if(t1p<t2p){
		return -1;
	}
	if(t1p == t2p){
		return 0;
	}
}

void addTime(int *ansp, int *t1p, int *t2p){
	int s1 = getSecondsFromTime(t1p), s2 = getSecondsFromTime(t2p);
	
	makeTimeFromSeconds(ansp, s1+s2);
}
	
	
void mulTime(int *ansp, int *tp, int n){
	int s = getSecondsFromTime(tp);
	makeTimeFromSeconds(ansp, n*s);
	
}

void divTime(int *ansp, int *tp, int n){
		int s = getSecondsFromTime(tp);
	makeTimeFromSeconds(ansp, s/n);
}
	
int divTimeByTime(int *t1p, int *t2p){
	int a = 0;
	a=(getSecondsFromTime(t1p))/(getSecondsFromTime(t2p));
return a;
}

void subTime(int *ansp, int *t1p, int *t2p){
		int s1 = getSecondsFromTime(t1p), s2 = getSecondsFromTime(t2p);
	makeTimeFromSeconds(ansp, abs(s1-s2));
}